use std::net::{TcpListener, TcpStream};
use std::thread;
use std::fs;
use std::io::{Read, Write, Error};

fn handle_client(mut stream: TcpStream) -> Result<(), Error>
{
    println!("Incoming connection from: {}", stream.peer_addr()?);
    let mut buf = [0; 512];
    loop 
    {
        let bytes_read = stream.read(&mut buf)?;
        if bytes_read == 0 { return Ok(()) }
        stream.write(&buf[..bytes_read])?;
    }
}

fn main() 
{
    // println!("Hello, world!");
    // 1. Let OS assign port
    let listener = TcpListener::bind("127.0.0.1:0").expect("Could not bind");
    // 2. Write the server port in "server_port" file
    fs::write("../server_port", listener.local_addr().unwrap().port().to_string());

    for stream in listener.incoming()
    {
        match stream 
        {
            Err(e) => {
                eprintln!("failed: {}", e)
            }
            Ok(stream) => {
                thread::spawn(move || {
                    handle_client(stream).unwrap_or_else(|error| eprintln!("{:?}", error));
                });
            }
        }
    }


    // 3. Launch client process

    // 4. Try communicating with client process
}
