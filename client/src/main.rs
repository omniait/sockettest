use std::net::TcpStream;
use std::str;
use std::io::{self, BufRead, BufReader, Write};

use std::fs::File;
use std::io::prelude::*;

fn main() 
{
    let mut file = File::open("../server_port").expect("Check that the server is running");
    let mut port = String::new();
    file.read_to_string(&mut port);
    let address = "127.0.0.1:".to_string() + &port;

    let mut stream = TcpStream::connect(address).expect("Could not connect to server");
    loop 
    {
        let mut input = String::new();
        let mut buffer: Vec<u8> = Vec::new();
        io::stdin().read_line(&mut input).expect("Failed to read from stdin");
        stream.write(input.as_bytes()).expect("Failed to write to server");

        let mut reader = BufReader::new(&stream);

        reader.read_until(b'\n', &mut buffer).expect("Could not read into buffer");
        print!("{}", str::from_utf8(&buffer).expect("Could not write buffer as string"));
    }
}
